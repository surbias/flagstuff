//
//  AdressBook.h
//  AddressBook
//
//  Created by Formando FLAG on 26/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressCard.h"
@interface AddressBook : NSObject

@property(readonly) NSString *bookName;
@property NSMutableArray *cards;

- (instancetype) initWithBookName:(NSString *)aBookName;
+ (instancetype) bookWith:(NSString *) aBookName;

- (void) addCard:(AddressCard *)aCard;
- (void) removeCard:(AddressCard *)aCard;

//Returns a specific card
- (AddressCard *) lookup:(NSString *) aName;

//returns the number of cards in the address book
- (NSUInteger) entries;

//calls description for each address card in the book
- (void) list;

@end
