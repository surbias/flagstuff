//
//  AddressCard.m
//  AddressBook
//
//  Created by Formando FLAG on 26/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "AddressCard.h"

@implementation AddressCard

// This is no longer needed on since XCode6
//@synthesize name = _name;
//@synthesize email = _email;
//@synthesize phone = _phone;


- (instancetype) initWithName:(NSString *) aName
                     andEmail:(NSString *) anEmail
                     andPhone:(NSString *) aPhone{
    self = [super init];
    
    if (self != nil) {
        _name = aName;
        _email = anEmail;
        _phone = aPhone;
    }
    return self;
}


+ (instancetype)cardWithName:(NSString *)aName
                    andEmail:(NSString *)anEmail
                    andPhone:(NSString *)aPhone{

    return [[AddressCard alloc]initWithName:aName
                            andEmail:anEmail
                            andPhone:aPhone];
}

//Getters
//- (NSString *)name{
//    return _name;
//}
//- (NSString *)phone{
//    return _phone;
//}
//- (NSString *)email{
//    return _email;
//}


- (NSString *)description{
    return [NSString stringWithFormat:@" name: %@ \n email: %@ \n phone: %@",
            _name, _email, _phone];
}


@end
