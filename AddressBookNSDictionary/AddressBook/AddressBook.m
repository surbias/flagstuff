//
//  AdressBook.m
//  AddressBook
//
//  Created by Formando FLAG on 26/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "AddressBook.h"

@implementation AddressBook

- (instancetype) initWithBookName:(NSString *)aBookName{
    self = [super init];
    
    if (self) {
        _bookName = aBookName;
        _cards = [[NSMutableDictionary alloc]init];
    }
    return self;
}

+ (instancetype) bookWith:(NSString *) aBookName{
    return [[AddressBook alloc]initWithBookName:aBookName];
}

- (void) addCard:(AddressCard *)aCard{
    [_cards setObject:aCard forKey:aCard.name];
}
- (void) removeCard:(AddressCard *)aCard{
    [_cards removeObjectForKey:aCard.name];
}

//Returns a specific card
- (AddressCard *) lookup:(NSString *) aName{
    return [_cards objectForKey:aName];
}

//returns the number of cards in the address book
- (NSUInteger) entries{
    return [_cards count];
}

//calls description for each address card in the book
- (void) list{
    //Note: if the content is not of type AddressCard, by default it returns the key
    for (AddressCard *card in [_cards allValues]) {
        NSLog(@"%@", card);
    }

}

- (NSString *)description{
    return [NSString stringWithFormat:@"%@ - %lu",  _bookName, [self entries]];
}

@end
