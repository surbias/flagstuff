//
//  AddressCard.h
//  AddressBook
//
//  Created by Formando FLAG on 26/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressCard : NSObject
//{
////    ivars
//    NSString *_name;
//    NSString *_phone;
//    NSString *_email;
//}

//@property does the following:
// - Declares ivars
// - Declares Getters and Setters
// - Implements Getters and Setters

@property NSString *name;
@property NSString *email;
@property NSString *phone;

- (instancetype) initWithName:(NSString *) aName
                     andEmail:(NSString *) anEmail
                     andPhone:(NSString *) aPhone;

//    convenience constructor
+ (instancetype) cardWithName:(NSString *) aName
                     andEmail:(NSString *) anEmail
                     andPhone:(NSString *) aPhone;

//Getters
//- (NSString *)name;
//- (NSString *)phone;
//- (NSString *)email;


@end
