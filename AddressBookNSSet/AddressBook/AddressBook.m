//
//  AdressBook.m
//  AddressBook
//
//  Created by Formando FLAG on 26/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "AddressBook.h"

@implementation AddressBook

- (instancetype) initWithBookName:(NSString *)aBookName{
    self = [super init];
    
    if (self) {
        _bookName = aBookName;
        _cards = [[NSMutableSet alloc]init];
    }
    return self;
}

+ (instancetype) bookWith:(NSString *) aBookName{
    return [[AddressBook alloc]initWithBookName:aBookName];
}

- (void) addCard:(AddressCard *)aCard{
    [_cards addObject:aCard];
}
- (void) removeCard:(AddressCard *)aCard{
    [_cards removeObject:aCard];
}

//Returns a specific card
- (AddressCard *) lookup:(NSString *) aName{
    for (AddressCard *card in _cards) {
        if ([card.name isEqualToString:aName]) {
            return card;
        }
    }
    return nil;
}

//returns the number of cards in the address book
- (NSUInteger) entries{
    return [_cards count];
}

//calls description for each address card in the book
- (void) list{
    for (AddressCard *card in _cards) {
        NSLog(@"%@", card);
    }
}

- (NSString *)description{
    return [NSString stringWithFormat:@"%@ - %lu",  _bookName, [self entries]];
}

@end
