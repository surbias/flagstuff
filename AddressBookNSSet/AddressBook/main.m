//
//  main.m
//  AddressBook
//
//  Created by Formando FLAG on 26/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressCard.h"
#import "AddressBook.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
//        Address Card instance class
        AddressCard *addressCard = [[AddressCard alloc] initWithName:@"Surbias"
                                                            andEmail:@"filipe.lmd@gmail.com"
                                                            andPhone:@"918473874"];
        
        AddressCard *addressCard2 = [AddressCard cardWithName:@"El Surbias"
                                                     andEmail:@"surbias@gmail.com"
                                                     andPhone:@"919191918"];
        NSLog(@"%@", addressCard);
        
        [addressCard2 setName:@"surbias"];
        NSLog(@"%@", addressCard2);
        
        AddressBook *addressBook = [AddressBook bookWith:@"Agenda"];
        
        [addressBook addCard:addressCard];
        [addressBook addCard:addressCard2];
        
        NSLog(@"%@", addressBook);
        NSLog(@"---------Lista-----------");
        [addressBook list];
        
        NSLog(@"---------Lista 2-----------");
        [addressBook removeCard:addressCard];
        [addressBook list];
        
        
        
    }
    return 0;
}
