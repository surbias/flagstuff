//
//  ViewController.h
//  AudioVideo
//
//  Created by Formando FLAG on 09/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)playAudio:(id)sender;
- (IBAction)playVideo:(id)sender;

@end

