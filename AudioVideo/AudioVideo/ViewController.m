//
//  ViewController.m
//  AudioVideo
//
//  Created by Formando FLAG on 09/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "ViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController ()

@property(nonatomic, strong) AVAudioPlayer *player;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)playAudio:(id)sender {
    
    //get resource url
    NSString *path = [[NSBundle mainBundle]pathForResource:@"audio" ofType:@"aif"];
    
    NSURL *audioURL = [NSURL fileURLWithPath:path];
    
    NSError *error;
    
    _player = [[AVAudioPlayer alloc]initWithContentsOfURL:audioURL error:&error];
    
    [_player play];
}

- (IBAction)playVideo:(id)sender {
    
    
    //get resource url
    NSString *path = [[NSBundle mainBundle]pathForResource:@"video" ofType:@"m4v"];
    
    NSURL *videoURL = [NSURL fileURLWithPath:path];
    
    //init view controller for videos
    MPMoviePlayerViewController *movieVC = [[MPMoviePlayerViewController alloc]initWithContentURL:videoURL];
    
    //present it as a modal
    [self presentViewController:movieVC animated:YES completion:nil];
}
@end
