//
//  main.m
//  Blocks
//
//  Created by Formando FLAG on 09/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        //without the __block, it wouldnt save the offset = 1 assignation
        //because when the block is declared it saves the current scope values
//        __block int offset = 100;
//        __block int xpto = 0;
//        NSUInteger(^block01)(NSUInteger, NSUInteger) = ^(NSUInteger x, NSUInteger y)
//        {
//            xpto = 10;
//            return (x +xpto) *(y + y);
//        };
//        
//        offset = 1;
//        
//        NSLog(@"%lu", block01(offset,30));
        
        NSArray *array = @[@"Um", @"Dois", @"Três"];
        array = [array sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSString *str1 = obj1;
            NSString *str2 = obj2;
            
            if ([str1 length] > [str2 length]) {
                return NSOrderedAscending;
            } else if([str1 length] == [str2 length]){
                return NSOrderedSame;
            } else{
                return NSOrderedDescending;
            }
        }];
        
        NSLog(@"%@", array);
    }
    return 0;
}
