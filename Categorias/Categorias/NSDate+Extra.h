//
//  NSDate+Extra.h
//  Categorias
//
//  Created by Formando FLAG on 09/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extra)

//return a string with the format dd/MM/YY
- (NSString *)shortDateStr;

@end
