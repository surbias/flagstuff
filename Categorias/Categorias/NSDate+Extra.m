
//
//  NSDate+Extra.m
//  Categorias
//
//  Created by Formando FLAG on 09/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "NSDate+Extra.h"

@implementation NSDate (Extra)

- (NSString *)shortDateStr{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    
    [formatter setDateFormat:@"dd/MM/YY"];
    
    return [formatter stringFromDate:self];
}

@end
