//
//  NSString+Extra.h
//  Categorias
//
//  Created by Formando FLAG on 09/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extra)
//constructor
+ (instancetype)emptyString;

//Method
- (BOOL)isEmpty;
@end
