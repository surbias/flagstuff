//
//  NSString+Extra.m
//  Categorias
//
//  Created by Formando FLAG on 09/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "NSString+Extra.h"

@implementation NSString (Extra)

+ (NSString *)emptyString{
    return @"";
}

- (BOOL)isEmpty{
    return [self length] == 0;
}

@end
