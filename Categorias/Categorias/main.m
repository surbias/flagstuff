//
//  main.m
//  Categorias
//
//  Created by Formando FLAG on 09/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Extra.h"
#import "NSDate+Extra.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSString *xpto = [NSString emptyString];
        NSLog(@"IsEmpty -> %@", [xpto isEmpty] ? @"Yes": @"No");
        NSLog(@"Hello, World!");
        
        NSDate *date = [NSDate date];
        
        NSLog(@"%@", [date shortDateStr]);
    }
    return 0;
}
