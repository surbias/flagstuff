//
//  ViewController.h
//  CoreAnimation
//
//  Created by Formando FLAG on 02/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *placeCheese;

@property (weak, nonatomic) IBOutlet UIImageView *fabricBottom;
@property (weak, nonatomic) IBOutlet UIImageView *fabricTop;

@property (weak, nonatomic) IBOutlet UIImageView *doorBottom;
@property (weak, nonatomic) IBOutlet UIImageView *doorTop;

@property (weak, nonatomic) IBOutlet UIImageView *bugView;


@end

