//
//  ViewController.m
//  CoreAnimation
//
//  Created by Formando FLAG on 02/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _bugView.center = CGPointMake(230, 250);

    
    //Change door top's frame
    CGRect newFrame = _doorTop.frame;
    newFrame.origin.y -= newFrame.size.height;
    
    //Change door bot's frame
    CGRect newFrame2 = _doorBottom.frame;
    newFrame2.origin.y += newFrame2.size.height;
    
    //Animate Doors
    [UIView animateWithDuration:2.0
                          delay:.2
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         _doorTop.frame = newFrame;
                         _doorBottom.frame = newFrame2;
                     }
                     completion:nil];
    
    //Animate Fabrics
    [UIView animateWithDuration:2.0
                          delay:.8
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         _fabricTop.frame = newFrame;
                         _fabricBottom.frame = newFrame2;
                     }
                     completion:^(BOOL finished){
                         [self moveLeft];
                     }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) turnLeft{
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        _bugView.transform = CGAffineTransformMakeRotation(0);
    } completion:^(BOOL finished){
//        [self moveRight];
    }];

}

- (void) turnRight{
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        _bugView.transform = CGAffineTransformMakeRotation(M_PI);
    } completion:^(BOOL finished){
        [self moveRight];
    }];
}

- (void) moveLeft{
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        _bugView.center = CGPointMake(75, 200);
    } completion:^(BOOL finished){
        [self turnRight];
    }];
}

- (void) moveRight{
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        _bugView.center = CGPointMake(230, 200);
    } completion:^(BOOL finished){
        [self turnLeft];
    }];
    
}

@end
