//
//  ViewController.h
//  CoreLocation
//
//  Created by Formando FLAG on 18/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (weak, nonatomic) IBOutlet UILabel *northRealLabel;
@property (weak, nonatomic) IBOutlet UILabel *northMagneticLabel;

- (IBAction)toggleLocation:(UISwitch *)sender;


@end

