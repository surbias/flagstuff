//
//  ViewController.m
//  CoreLocation
//
//  Created by Formando FLAG on 18/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController () <CLLocationManagerDelegate>
@property(strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //instance location manager
    _locationManager = [[CLLocationManager alloc]init];
    
    //request user's location authorization
    [_locationManager requestWhenInUseAuthorization];
    
    _locationManager.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleLocation:(UISwitch *)sender {
    if (sender.on) {
        [_locationManager startUpdatingLocation];
        [_locationManager startUpdatingHeading];
    } else {
        [_locationManager stopUpdatingLocation];
        [_locationManager stopUpdatingHeading];
        
        _longitudeLabel.text = @"...";
        _latitudeLabel.text = @"...";
        _northMagneticLabel.text = @"...";
        _northRealLabel.text = @"...";
        _speedLabel.text = @"...";
    }
}

# pragma mark - CLLocationManagerDelegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *currentLocation = locations[0];
    
    _longitudeLabel.text = [NSString stringWithFormat:@"%f", currentLocation.coordinate.longitude];
    _latitudeLabel.text = [NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude];
    _speedLabel.text = [NSString stringWithFormat:@"%.2f km/h", currentLocation.speed / 3.6];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading{
    
    _northRealLabel.text = [NSString stringWithFormat:@"%f", newHeading.trueHeading];
    _northMagneticLabel.text = [NSString stringWithFormat:@"%f", newHeading.magneticHeading];
    
}
@end
