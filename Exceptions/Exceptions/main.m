//
//  main.m
//  Exceptions
//
//  Created by Formando FLAG on 09/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSArray *array = @[@1.0, @2.0, @3.0];
        
        @try {
            array[5];
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception);
            
            @throw [NSException exceptionWithName:@"My mistake" reason:@"Bad stuff happened" userInfo:nil];
        }
//        @finally {
//            <#Code that gets executed whether or not an exception is thrown#>
//        }
        
    }
    return 0;
}
