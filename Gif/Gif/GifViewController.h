//
//  GifViewController.h
//  Gif
//
//  Created by Formando FLAG on 16/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GifViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UIImageView *image;
//- (IBAction)sliderChanged:(UISlider *)sender;
//- (IBAction)stepperChanged:(UIStepper *)sender;

- (IBAction)valueChanged:(id)sender;

@end
