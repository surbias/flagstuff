//
//  GifViewController.m
//  Gif
//
//  Created by Formando FLAG on 16/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "GifViewController.h"

@interface GifViewController ()

@end

@implementation GifViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _label.text = [NSString stringWithFormat:@"%d", (int)_stepper.value];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//- (IBAction)sliderChanged:(UISlider *)sender {
//    _image.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.gif", (int)sender.value]];
//    _label.text = [NSString stringWithFormat:@"%d", (int)sender.value];
//    _stepper.value = sender.value;
//}
//
//- (IBAction)stepperChanged:(UIStepper *)sender {
//    _image.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.gif", (int)sender.value]];
//    _label.text = [NSString stringWithFormat:@"%d", (int)sender.value];
//    _slider.value = sender.value;
//}

- (IBAction)valueChanged:(id)sender {
    int value;
    
    if ([sender isKindOfClass:[UISlider class]]) {
        value = ((UISlider *)sender).value;
        _slider.value = value;
        
    } else if([sender isKindOfClass:[UIStepper class]]) {
        value = ((UIStepper *)sender).value;
        _slider.value = value;
    } else {
        return;
    }
    
    _image.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.gif", value]];
    _label.text = [NSString stringWithFormat:@"%d", value];
    
}


@end
