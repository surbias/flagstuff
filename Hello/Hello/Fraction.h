//
//  Fraction.h
//  Hello
//
//  Created by Formando FLAG on 21/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction : NSObject{
//    Instance Variables (ivars)
    int _numerator;
    int _denominator;
}

//    Convenience Constructor
+ (instancetype)fractionWithNumerator:(int)aNumerator
                        andDenominator:(int)aDenominator;

//    Init
- (instancetype)initWithNumerator:(int)aNumerator
                   andDenominator:(int)aDenominator;

//    Getters
- (int)numerator;
- (int)denominator;

//    Setters
- (void)setNumerator:(int) aNumerator;
- (void)setDenominator:(int) aDenominator;

//    Misc.
- (void)reduce;
- (double)convertToNum;
- (BOOL) isInteger;

@end
