//
//  Fraction.m
//  Hello
//
//  Created by Formando FLAG on 21/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "Fraction.h"

@implementation Fraction


+ (instancetype)fractionWithNumerator:(int)aNumerator
                       andDenominator:(int)aDenominator{
    return [[Fraction alloc] initWithNumerator:aNumerator
                                andDenominator:aDenominator];
}

- (instancetype)initWithNumerator:(int)aNumerator
                   andDenominator:(int)aDenominator{
    
    self = [super init];
    
    if (self != nil) {
        //        Initializing the ivars
        _numerator = aNumerator;
        _denominator = aDenominator;
    }
    
    return self;
}

//    Getters
- (int) numerator{
    return _numerator;
}

- (int) denominator{
    return _denominator;
}

//    Setters
- (void)setNumerator:(int)aNumerator{
    _numerator = aNumerator;
}

- (void)setDenominator:(int)aDenominator{
    _denominator = aDenominator;
}

- (NSString *)description{
    return[[NSString alloc]initWithFormat:@"%d/%d", _numerator, _denominator];
}

- (void)reduce{}
- (double)convertToNum{
    return (double)_numerator/(double)_denominator;
}
- (BOOL) isInteger{
    return _numerator%_denominator == 0;
}

@end
