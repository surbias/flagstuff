//
//  MyObject.h
//  Hello
//
//  Created by Formando FLAG on 21/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyObject : NSObject

@property(strong, nonatomic)NSString *name;
@property(strong, nonatomic)NSNumber *age;

+ (NSString *)getName;

@end
