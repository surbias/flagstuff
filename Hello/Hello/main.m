//
//  main.m
//  Hello
//
//  Created by Formando FLAG on 21/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"
#import "Fraction.h"
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
//        Variable that saves an instance of class Fraction
        Fraction *frac01 = [[Fraction alloc]init];
        
        
//        Calling the Setter
        [frac01 setNumerator:2];
        [frac01 setDenominator:10];
        
//        NSLog(@"%d/%d", [frac01 numerator], [frac01 denominator]);
        
        Fraction *fraction = [[Fraction alloc]initWithNumerator:2 andDenominator:3];
        
        
//        by default printing an object calls its description
//        NSLog(@"%@",[fraction description]);
        NSLog(@"%@", fraction);
        
//        check if integer
        NSLog(@"Is %@ integer", ![fraction isInteger] ? @"not": @"");
        
//        check number with convertToNum
        NSLog(@"Convert to num: %f", [fraction convertToNum]);
//        With class-based convenience constructor
        Fraction *frac02 = [Fraction fractionWithNumerator:2 andDenominator:6];
        NSLog(@"%@", frac02);
    }
    return 0;
}
