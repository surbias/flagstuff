//
//  TestViewController.h
//  HelloWorld
//
//  Created by Formando FLAG on 11/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtField;
@property (weak, nonatomic) IBOutlet UILabel *txtLabel;

- (IBAction)go;
@end
