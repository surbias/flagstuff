//
//  Favorite.m
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 19/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "Favorite.h"
#import "AppDelegate.h"
#import "Spot.h"


@implementation Favorite

@dynamic spotIdentifier;
@dynamic dateAdded;

+ (instancetype)favoriteWithSpotIdentifier:(int32_t)aSpotIdentifier{
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *objectContext = appDelegate.managedObjectContext;
    
    //Create a new instance at core data
    Favorite *aFavorite = [NSEntityDescription insertNewObjectForEntityForName:@"Favorite" inManagedObjectContext:objectContext];
    
    if (aFavorite){
        aFavorite.spotIdentifier = aSpotIdentifier;
        aFavorite.dateAdded = [[NSDate date] timeIntervalSinceReferenceDate];
    }
    
    //commits initialization
    [appDelegate saveContext];
    
    return aFavorite;
}

+ (BOOL)hasFavoriteWithidentifier:(int32_t)anIdentifer{
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *objectContext = appDelegate.managedObjectContext;
    
    NSError *error;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Favorite"];
    
    fetch.fetchLimit = 1;
    fetch.predicate = [NSPredicate predicateWithFormat:@"spotIdentifier == %d", anIdentifer];
    
    NSArray *results = [objectContext executeFetchRequest:fetch error:&error];
    
    if (error != nil) {
        NSLog(@"Error: %@", [error localizedDescription]);
        return NO;
    }
    
    if ([results count] != 0) {
        return YES;
    } else{
        return NO;
    }
}

+ (void)removeFavoriteWithIdentifer:(int32_t)anIdentifier{
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *objectContext = appDelegate.managedObjectContext;
    
    NSError *error;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Favorite"];
    
    fetch.fetchLimit = 1;
    fetch.predicate = [NSPredicate predicateWithFormat:@"spotIdentifier == %d", anIdentifier];
    
    Favorite *aFavorite = [objectContext executeFetchRequest:fetch error:&error][0];
    
    if (error != nil) {
        NSLog(@"Error: %@", [error localizedDescription]);
        return;
    }
    
    if (aFavorite) {
        [objectContext deleteObject:aFavorite];
        [appDelegate saveContext];
    } else{
        return;
    }
}

+ (NSArray *)fetchAllFavorites{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *objectContext = appDelegate.managedObjectContext;
    
    NSError *error;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Favorite"];
    
    NSArray *results = [objectContext executeFetchRequest:fetch error:&error];
    
    if (error != nil) {
        NSLog(@"Error: %@", [error localizedDescription]);
        return nil;
    }
    
    return results;
}

+ (void)removeAllFavorites{
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *objectContext = appDelegate.managedObjectContext;
    
    for (Favorite *aFavorite in [Favorite fetchAllFavorites]) {
        [objectContext deleteObject:aFavorite];
    }
    
    [appDelegate saveContext];
}

+(NSArray *)fetchAllFavoriteSpots{
    NSMutableArray *tmp = [@[] mutableCopy];
    for (Favorite *aFavorite in [Favorite fetchAllFavorites]) {
        Spot *aSpot = [Spot fetchSpotWithIdentifier:aFavorite.spotIdentifier];
        if (aSpot) {
            [tmp addObject:aSpot];
        }
    }
    return [tmp copy];
}

@end
