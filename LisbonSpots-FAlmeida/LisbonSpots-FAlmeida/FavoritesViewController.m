//
//  FavoritesViewController.m
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 19/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "FavoritesViewController.h"
#import "DetailSpotViewController.h"
#import "Constants.h"
#import "Favorite.h"
#import "Constants.h"
#import "UIImage+ImageAsync.h"


@interface FavoritesViewController()<UICollectionViewDelegate, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UINavigationItem *NavigationItemTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property(strong, nonatomic) NSArray* favorites;

@end

@implementation FavoritesViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    _NavigationItemTitle.title = kFAVORITES_TITLE;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    _favorites = [Favorite fetchAllFavoriteSpots];
    [_collectionView reloadData];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    if ([_favorites count] == 0) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Information"
                                                          message:kMESSAGE_FAVORITES
                                                         delegate:self
                                                cancelButtonTitle:@"Gotcha!"
                                                otherButtonTitles: nil];
        [message show];
    }
}

#pragma Mark - UICollectionViewDelegate Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_favorites count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell;
    NSUInteger row = [indexPath row];
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCOLLECTION_CELL
                                                         forIndexPath:indexPath];
    
    UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:1];
    UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)[cell viewWithTag:2];
    UILabel *cellTitle = (UILabel *)[cell viewWithTag:3];
    
    Spot *aSpot = _favorites[row];
    
    [UIImage loadAsyncFromURL:aSpot.imageURL
                withImageView:cellImageView
                     withName:[NSString stringWithFormat:@"%d",aSpot.identifier]
         andActivityIndicator:indicator];
    
    cellTitle.text = aSpot.name;
    
    return cell;
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"goToDetail"]) {
        DetailSpotViewController *detail = segue.destinationViewController;
        
        NSIndexPath *path = _collectionView.indexPathsForSelectedItems[0];
        
        detail.selectedSpot = _favorites[path.row];
        
    }
}


@end
