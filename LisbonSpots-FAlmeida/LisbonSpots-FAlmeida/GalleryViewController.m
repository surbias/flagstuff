//
//  GalleryViewController.m
//  LisbonSpots-FAlmeida
//
//  Created by Formando FLAG on 14/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "GalleryViewController.h"
#import "DetailSpotViewController.h"
#import "Spot.h"
#import "UIImage+ImageAsync.h"
#import "Constants.h"

@interface GalleryViewController ()< UITableViewDataSource, UITableViewDelegate>
@property(strong, nonatomic) NSArray *barArray;
@property(strong, nonatomic) NSArray *restaurantArray;
@property(strong, nonatomic) NSArray *clubArray;
@property(strong, nonatomic) NSMutableDictionary *cellDictionary;

//header cell contents
@property (weak, nonatomic) IBOutlet UIImageView *headerIcon;
@property (weak, nonatomic) IBOutlet UILabel *headerTitle;


//self.table is not intrisic so I need to
//use this property instead
@property (weak, nonatomic) IBOutlet UITableView *galleryTable;
@property (weak, nonatomic) IBOutlet UINavigationItem *titleNavigationItem;

@end

@implementation GalleryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _titleNavigationItem.title = kGALLERY_TITLE;
    
    _barArray = [Spot fetchAllSpotsWithType:@"bar"];
    _restaurantArray = [Spot fetchAllSpotsWithType:@"restaurant"];
    _clubArray = [Spot fetchAllSpotsWithType:@"club"];
    _cellDictionary = [NSMutableDictionary dictionary];
    
    [_galleryTable setDataSource:self];
    [_galleryTable setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self spotsArrayForSection:section] count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UITableViewCell *headerCell= [tableView dequeueReusableCellWithIdentifier:kCELL_HEADER];
    
    Spot *spot = [self spotsArrayForSection:section][0];
    
    UIImageView *iconImage = (UIImageView *)[headerCell viewWithTag:0];
    UILabel *headerTitle = (UILabel *)[headerCell viewWithTag:1];

    headerCell.backgroundColor = [Spot getColorSpot:spot];
    headerCell.contentView.backgroundColor = [Spot getColorSpot:spot];
    iconImage.image = [Spot getTypeIconFromSpot:spot];
    headerTitle.text = [spot.type capitalizedString];
    
    return headerCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    
    cell = [tableView dequeueReusableCellWithIdentifier:kCELL_NORMAL forIndexPath:indexPath];
    
    
    NSArray *spots = [self spotsArrayForSection:section];
    Spot *spot = spots[row];
    
    UILabel *cellTitle = (UILabel *)[cell viewWithTag:1];
    UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:2];
    UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)[cell viewWithTag:3];
    
    cellTitle.text = spot.name;

    [UIImage loadAsyncFromURL:spot.imageURL
                withImageView:cellImageView
                     withName:[NSString stringWithFormat:@"%d",spot.identifier]
         andActivityIndicator:indicator];
    
    return cell;
}


#pragma Mark - Helper methods
- (NSArray *)spotsArrayForSection:(NSUInteger)section{
    
    switch (section) {
        case 0:
            return _barArray;
        case 1:
            return _restaurantArray;
        case 2:
            return _clubArray;
    }
    return nil;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"goToDetail"]) {
        DetailSpotViewController *detail = segue.destinationViewController;
        
        NSUInteger row = _galleryTable.indexPathForSelectedRow.row;
        NSUInteger section = _galleryTable.indexPathForSelectedRow.section;
        
        detail.selectedSpot = [self spotsArrayForSection:section][row];
        
    }
}


@end
