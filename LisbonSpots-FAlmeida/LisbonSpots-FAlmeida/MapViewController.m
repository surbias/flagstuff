//
//  MapViewController.m
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 16/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "MapViewController.h"
#import "Spot.h"
#import "Constants.h"
#import "NSUserDefaults+MapConfiguration.h"


@interface MapViewController()

@property (weak, nonatomic) IBOutlet UINavigationItem *titleNavigationItem;
@property (weak, nonatomic) IBOutlet UISwitch *locationSwitcher;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *centerButton;

@property(strong, nonatomic) CLLocationManager *locationManager;
@property BOOL userLocation;

- (IBAction)sliderChanged:(UISlider *)sender;
- (IBAction)centerUserLocation;

@end

@implementation MapViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    _titleNavigationItem.title = kMAP_TITLE;
    
    [_centerButton setImage:[UIImage imageNamed:@"IconNear"]
                   forState:UIControlStateNormal];
    
    _locationManager = [[CLLocationManager alloc]init];
    [_locationManager requestWhenInUseAuthorization];
    
    [_mapView addAnnotations:[Spot fetchAllSpots]];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    _userLocation = [userDefaults boolForKey:@"userLocation"];
    _locationManager.distanceFilter = [NSUserDefaults distanceFilterWithIdentifier:(NSUInteger)[userDefaults integerForKey:@"mapDistanceFilter"]];
    _locationManager.desiredAccuracy = [NSUserDefaults mapAccuracyWithIdentifier:(NSUInteger)[userDefaults integerForKey:@"mapAccuracy"]];

    _mapView.mapType = [NSUserDefaults mapTypeWithIdentifier:(NSUInteger)[userDefaults integerForKey:@"mapType"]];
    [self checkForUserLocation];
    [self setMapRegion];
}

- (IBAction)sliderChanged:(UISlider *)sender {

    CGFloat value = sender.maximumValue - sender.value + sender.minimumValue;
    _mapView.region = MKCoordinateRegionMakeWithDistance(_mapView.centerCoordinate, value * 1000, value * 1000);
}

- (IBAction)centerUserLocation {
    [self checkForUserLocation];
}

#pragma Mark - MKMapViewDelegate Methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    
    //So that it does not change the default styling for user location
    if([annotation isKindOfClass:[MKUserLocation class]]){
        return nil;
    }
    
    MKAnnotationView *annotationView = [[MKAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"spotAnnotation"];

    annotationView.canShowCallout = YES;
    
    if ([annotation isKindOfClass:[Spot class]]) {
        Spot *spot = (Spot *)annotation;
        annotationView.image = [Spot getTypeIconFromSpot:spot];
    }
    
    return annotationView;
}

#pragma Mark - Helper Methods
- (void)checkForUserLocation{
    
    _mapView.showsUserLocation = _userLocation;
    _centerButton.enabled = _userLocation;
    
    if (_userLocation) {
        _mapView.centerCoordinate = _mapView.userLocation.coordinate;
        _centerButton.tintColor = kCOLOR_TINT;

    }else{
        _centerButton.tintColor = [UIColor grayColor];
    }
}

- (void)setMapRegion{
    CLLocationCoordinate2D location = kCOORDINATES_ATRIUM_SALDANHA;
    MKCoordinateRegion viewRegion, adjustedRegion;
    if (_userLocation) {
        [self checkForUserLocation];
    }else {
        _mapView.centerCoordinate = location;
    }
    
    //set default region
    viewRegion = MKCoordinateRegionMakeWithDistance(location, kREGION_AXIS, kREGION_AXIS);
    adjustedRegion = [_mapView regionThatFits:viewRegion];
    
    [_mapView setRegion:adjustedRegion];
}

@end
