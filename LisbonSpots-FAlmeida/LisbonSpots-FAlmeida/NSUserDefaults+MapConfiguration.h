//
//  NSUserDefaults+MapConfiguration.h
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 19/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (MapConfiguration)

+ (NSUInteger)mapTypeWithIdentifier:(NSUInteger)identifier;
+ (NSUInteger)mapAccuracyWithIdentifier:(NSUInteger)identifier;
+ (NSUInteger)distanceFilterWithIdentifier:(NSUInteger)identifier;
    
@end
