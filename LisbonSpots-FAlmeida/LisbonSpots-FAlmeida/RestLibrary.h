//
//  RestLibrary.h
//  LisbonSpots-FAlmeida
//
//  Created by Formando FLAG on 14/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestLibrary : NSObject

//returns service url
+ (NSURL *)spotsUrl;

//Reads spots, parses them (as json)
//and inserts them in CoreData
+ (BOOL) getSpots;

@end
