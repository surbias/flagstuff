//
//  SettingsViewController.m
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 19/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "SettingsViewController.h"
#import "Constants.h"
#import "NSUserDefaults+MapConfiguration.h"
#import "Favorite.h"

@interface SettingsViewController()<UIAlertViewDelegate>

@property(strong, nonatomic) NSUserDefaults *userDefaults;

@property (weak, nonatomic) IBOutlet UINavigationItem *NavigationItemTitle;

@property (weak, nonatomic) IBOutlet UISwitch *userLocationSwitch;
@property (weak, nonatomic) IBOutlet UISegmentedControl *mapTypeSegmented;
@property (weak, nonatomic) IBOutlet UISegmentedControl *precisionSegmented;
@property (weak, nonatomic) IBOutlet UISegmentedControl *distanceSegmented;

- (IBAction)toggleUserLocation:(UISwitch *)sender;
- (IBAction)mapStyleChanged:(UISegmentedControl *)sender;
- (IBAction)mapPrecisionChanged:(UISegmentedControl *)sender;
- (IBAction)mapDistanceFilterChanged:(UISegmentedControl *)sender;

- (IBAction)removeAllFavorites:(UIButton *)sender;

@end

@implementation SettingsViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    _NavigationItemTitle.title = kSETTINGS_TITLE;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    _userDefaults = [NSUserDefaults standardUserDefaults];
    _userLocationSwitch.on = [_userDefaults boolForKey:@"userLocation"];
    _mapTypeSegmented.selectedSegmentIndex = [_userDefaults integerForKey:@"mapType"];
    _precisionSegmented.selectedSegmentIndex = [_userDefaults integerForKey:@"mapAccuracy"];
    _distanceSegmented.selectedSegmentIndex = [_userDefaults integerForKey:@"mapDistanceFilter"];
}

- (IBAction)toggleUserLocation:(UISwitch *)sender {
    [_userDefaults setBool:sender.on forKey:@"userLocation"];
}

- (IBAction)mapStyleChanged:(UISegmentedControl *)sender {
    [_userDefaults setInteger:sender.selectedSegmentIndex forKey:@"mapType"];
}

- (IBAction)mapPrecisionChanged:(UISegmentedControl *)sender {
    [_userDefaults setInteger:sender.selectedSegmentIndex forKey:@"mapAccuracy"];
}

- (IBAction)mapDistanceFilterChanged:(UISegmentedControl *)sender {
    [_userDefaults setInteger:sender.selectedSegmentIndex forKey:@"mapDistanceFilter"];
}

- (IBAction)removeAllFavorites:(UIButton *)sender {
    UIAlertView *message;
    if ([[Favorite fetchAllFavorites] count] != 0){
        message = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                      message:kPROMPT_SETTINGS
                                                     delegate:self
                                            cancelButtonTitle:@"Yes"
                                            otherButtonTitles:@"No", nil];
    } else {
        message = [[UIAlertView alloc] initWithTitle:@"Information"
                                             message:KBEFORE_PROMPT_SETTINGS
                                            delegate:self
                                   cancelButtonTitle:@"Woops!"
                                   otherButtonTitles:nil];
    }
    [message show];
}

#pragma Mark - UIAlertViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [Favorite removeAllFavorites];
    }
}

@end
