//
//  Spot.h
//  LisbonSpots-FAlmeida
//
//  Created by Formando FLAG on 14/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>

@interface Spot : NSManagedObject <MKAnnotation>

@property (nonatomic) int32_t identifier;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

//constructs object based on json object
+ (instancetype)spotWithDictionary:(NSDictionary *)aDictionary;


//Core Data Access Methods
+ (NSArray *)fetchAllSpots;
+ (NSArray *)fetchAllSpotsWithType:(NSString *)aType;
+ (Spot *)fetchSpotWithIdentifier:(int32_t)identifier;
+ (void)removeAllSpots;


//Spot utilities
+ (UIImage *) getTypeIconFromSpot:(Spot *)aSpot;
+ (UIColor *) getColorSpot:(Spot *) aSpot;

@end
