//
//  Spot.h
//  Mapkit-coredata
//
//  Created by Formando FLAG on 09/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>

@interface Spot : NSManagedObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic) NSTimeInterval dateCreated;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

// Construtor
+(instancetype) spotWithLat:(double)lat
                       long:(double)lon
                    andName:(NSString *)aName;


// Getters do MKAnnotation
-(CLLocationCoordinate2D) coordinate;
-(NSString *) title;
-(NSString *) subtitle;

//Core Data
+(NSArray *) allSpots;
+(void)removeSpot:(Spot *)aSpot;


@end
