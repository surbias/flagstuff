//
//  Spot.m
//  Mapkit-coredata
//
//  Created by Formando FLAG on 09/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "Spot.h"
#import "AppDelegate.h"

@implementation Spot

@dynamic name;
@dynamic dateCreated;
@dynamic latitude;
@dynamic longitude;

+ (instancetype)spotWithLat:(double)lat long:(double)lon andName:(NSString *)aName {
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *moc = appDelegate.managedObjectContext;
    
    //Create a new instance at core data
    Spot *newSpot = [NSEntityDescription insertNewObjectForEntityForName:@"Spot" inManagedObjectContext:moc];
    
    //Initialize newSpot's properties
    if (newSpot) {
    
        newSpot.latitude = lat;
        newSpot.longitude = lon;
        newSpot.name = aName;
    
    }
    
    //commits initialization
    [appDelegate saveContext];
    
    return newSpot;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ [%f, %f]", self.name, self.latitude, self.longitude];
}

// Getters do MKAnnotation
-(CLLocationCoordinate2D) coordinate {
    return CLLocationCoordinate2DMake(self.latitude, self.longitude);
}

-(NSString *)title {
    return self.name;
}

-(NSString *)subtitle {
    NSDate *tmp = [NSDate dateWithTimeIntervalSince1970:self.dateCreated];
    
    NSDateFormatter *dFormatter = [[NSDateFormatter alloc] init];
    dFormatter.dateStyle = NSDateFormatterShortStyle;
    
    return [dFormatter stringFromDate:tmp];
}

#pragma Core Data  - crud implementation

+ (NSArray *) allSpots {

    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *moc = appDelegate.managedObjectContext;
    
    NSError *error;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
    NSArray *results = [moc executeFetchRequest:fetch error:&error];
    
    if (error != nil) {
        NSLog(@"Error: %@", [error localizedDescription]);
        return nil;
    }
    
    return results;
    
}

+ (void)removeSpot:(Spot *)aSpot{
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *moc = appDelegate.managedObjectContext;
    
    [moc deleteObject:aSpot];
    
    [appDelegate saveContext];
    
}

@end
