//
//  ViewController.h
//  MapKit
//
//  Created by Formando FLAG on 24/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITextField *txtField;

- (IBAction)mapTypeChanged:(UISegmentedControl *)sender;
- (IBAction)toggleUserLoc:(UISwitch *)sender;
- (IBAction)zoomChanged:(UISlider *)sender;
- (IBAction)addAnnotation;
- (IBAction)removeAnnotation;
@end

