//
//  ViewController.m
//  MapKit
//
//  Created by Formando FLAG on 24/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "ViewController.h"
#import "Spot.h"

@interface ViewController () <MKMapViewDelegate, UITextFieldDelegate>

@property (nonatomic) CLLocationManager *clManager;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Instanciar o clManager
    _clManager = [[CLLocationManager alloc] init];
    
    // Pedir autorização para ter acesso à localização do user
    // Info.plist -> NSLocationWhenInUseUsageDescription
    [_clManager requestWhenInUseAuthorization];
    

    // Registar o ViewController como delegate da mapView
    _mapView.delegate = self;
    
    // Registar o ViewController como delegate da txtField
    _txtField.delegate = self;
    
    //Fetches all spots from core data
    [_mapView addAnnotations:[Spot allSpots]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)mapTypeChanged:(UISegmentedControl *)sender {

    switch (sender.selectedSegmentIndex) {
        case 0:
            _mapView.mapType = MKMapTypeStandard;
            break;
        case 1:
            _mapView.mapType = MKMapTypeHybrid;
            break;
        case 2:
            _mapView.mapType = MKMapTypeSatellite;
            break;
        default:
            break;
    }
    
}

- (IBAction)toggleUserLoc:(UISwitch *)sender {
    _mapView.showsUserLocation = sender.isOn;
}

- (IBAction)zoomChanged:(UISlider *)sender {

    // Inverter o slider
    float newVal = sender.maximumValue - sender.value;
    newVal += sender.minimumValue;
    
    NSLog(@"%.0f km", newVal);
    
    _mapView.region = MKCoordinateRegionMakeWithDistance(_mapView.centerCoordinate, newVal * 1000, newVal * 1000);
}

- (IBAction)addAnnotation {
    Spot *thisSpot = [Spot spotWithLat:_mapView.userLocation.coordinate.latitude
                                  long:_mapView.userLocation.coordinate.longitude
                               andName:_txtField.text];
    
    [_mapView addAnnotation:thisSpot];
    
    _txtField.text = @"";
    
    // Esconder o teclado
    [_txtField  resignFirstResponder];
}

- (IBAction)removeAnnotation {
    for (id<MKAnnotation> tmp in _mapView.selectedAnnotations) {
        if ([tmp isKindOfClass:[Spot class]]) {
            [_mapView removeAnnotation:tmp];
            [Spot removeSpot:tmp];
        }
    }
    
}

#pragma mark - MKMapViewDelegate Methods
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    // Centrar a mapView na coordenada do user
    [_mapView setCenterCoordinate:userLocation.location.coordinate animated:NO];
    
    //NSLog(@"%@", userLocation.location);
}

#pragma mark - UITextFieldDelegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    // Esconder o teclado
    [textField  resignFirstResponder];
    
    return NO;
}


@end
