//
//  Spot.h
//  Mapkit
//
//  Created by Formando FLAG on 18/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface Spot : NSObject <MKAnnotation>

@property(strong, nonatomic)NSString *name;
@property(strong, nonatomic)NSDate *dateCreated;

@property(nonatomic)double latitude;
@property(nonatomic)double longitude;

- (instancetype)initWithName:(NSString *)aName
                 andLatitude:(double)aLatitude
                andLongitude:(double)aLongitude;

@end
