//
//  Spot.m
//  Mapkit
//
//  Created by Formando FLAG on 18/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "Spot.h"

@implementation Spot

- (instancetype)initWithName:(NSString *)aName
                 andLatitude:(double)aLatitude
                andLongitude:(double)aLongitude{

    self = [super init];
    
    if (self) {
        _name = aName;
        _latitude = aLatitude;
        _longitude = aLongitude;
        _dateCreated = [NSDate date];
        
    }
    return self;
}

- (NSString *)title{
    return _name;
}
- (NSString *)subtitle{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    
    return [NSString stringWithFormat:@"Created at: %@", [dateFormatter stringFromDate:_dateCreated]];
}

- (CLLocationCoordinate2D)coordinate{
    return CLLocationCoordinate2DMake(_latitude, _longitude);
}

@end
