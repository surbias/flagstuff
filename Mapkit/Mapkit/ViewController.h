//
//  ViewController.h
//  Mapkit
//
//  Created by Formando FLAG on 18/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UISwitch *locationSwitcher;
@property (weak, nonatomic) IBOutlet UISlider *zoomSlider;
@property (weak, nonatomic) IBOutlet UITextField *annotationTextField;


- (IBAction)toggleUserLocation:(UISwitch *)sender;
- (IBAction)sliderChanged:(UISlider *)sender;

@end

