//
//  ViewController.m
//  Mapkit
//
//  Created by Formando FLAG on 18/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "ViewController.h"
#import "Spot.h"

@interface ViewController ()<MKMapViewDelegate, UITextFieldDelegate>
@property(strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _locationManager = [[CLLocationManager alloc]init];
    [_locationManager requestWhenInUseAuthorization];
    
    [self sliderChanged:_zoomSlider];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleUserLocation:(UISwitch *)sender {
    
}

- (IBAction)sliderChanged:(UISlider *)sender {
    
    
    CGFloat value = sender.maximumValue - sender.value + sender.minimumValue;
    
    _mapView.region = MKCoordinateRegionMakeWithDistance(_mapView.centerCoordinate, value * 1000, value * 1000);
}

#pragma Mark - MKMapViewDelegate Methods

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    
    if (_locationSwitcher.on) {
        _mapView.centerCoordinate = userLocation.coordinate;
    }
    
}

#pragma Mark - UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    //hide keyboard
    [textField resignFirstResponder];
    
    Spot *spot = [[Spot alloc]initWithName:textField.text
                               andLatitude:_mapView.userLocation.coordinate.latitude
                              andLongitude:_mapView.userLocation.coordinate.longitude];
    
    //add annotation
    [_mapView addAnnotation:spot];
    
    return YES;
    
}


@end
