//
//  FirstViewController.h
//  Modal
//
//  Created by Formando FLAG on 16/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *switchLabel;
@property (weak, nonatomic) IBOutlet UIButton *button;
- (IBAction)toggle:(UISwitch *)sender;
- (IBAction)nextViewController;


@end
