//
//  FirstViewController.m
//  Modal
//
//  Created by Formando FLAG on 16/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "FirstViewController.h"
#import "SecondViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)toggle:(UISwitch *)sender {
    
    if (sender.on) {
        _switchLabel.text = @"ON";
        _button.enabled = YES;
    } else {
        _switchLabel.text = @"OFF";
        _button.enabled = NO;
    }
}

- (IBAction)nextViewController {
    SecondViewController *secondViewController = [[SecondViewController alloc]initWithNibName:nil bundle:nil];
    
    [secondViewController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    //show SecondViewController as Modal
    [self presentViewController:secondViewController animated:YES completion:^{}];

}
@end
