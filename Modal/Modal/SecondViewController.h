//
//  SecondViewController.h
//  Modal
//
//  Created by Formando FLAG on 16/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

- (IBAction)close;

@end
