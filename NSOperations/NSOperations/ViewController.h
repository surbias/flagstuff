//
//  ViewController.h
//  NSOperations
//
//  Created by Formando FLAG on 02/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelThread1;

@property (weak, nonatomic) IBOutlet UIView *viewThread2;
@property (weak, nonatomic) IBOutlet UILabel *labelThread2;

- (IBAction)changeColor:(UIButton *)sender;

@end

