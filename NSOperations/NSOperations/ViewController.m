//
//  ViewController.m
//  NSOperations
//
//  Created by Formando FLAG on 02/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

//Creating threads through NSOperationQueue
@property(strong, nonatomic) NSOperationQueue *opQueue;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self task01];
    
    //Instance an operation queue
    _opQueue = [[NSOperationQueue alloc]init];
    
    //Instance an operation
    NSInvocationOperation *op01 = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(task01) object:nil];
    
    NSBlockOperation *op02 = [NSBlockOperation blockOperationWithBlock:^{
        for (NSUInteger x = 0; YES; x++) {
            NSString *txt = [NSString stringWithFormat:@"%d", x];
            [_labelThread2 performSelectorOnMainThread:@selector(setText:)
                                            withObject:txt
                                         waitUntilDone:YES];
            
            
            CGFloat green = (arc4random() % 255) / 255.0;
            CGFloat red = (arc4random() % 255) / 255.0;
            CGFloat blue = (arc4random() % 255) / 255.0;
            UIColor *randomColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
            usleep(100000);
            
            [_viewThread2 performSelectorOnMainThread:@selector(setBackgroundColor:)
                                           withObject:randomColor
                                        waitUntilDone:YES];
            
        }
    }];
    
    [_opQueue addOperation:op01];
    [_opQueue addOperation:op02];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeColor:(UIButton *)sender {
    UIColor *color = nil;
    
    switch (sender.tag) {
        case 1:
            color = [UIColor orangeColor];
            break;
        case 2:
            color = [UIColor redColor];
            break;
        case 3:
            color = [UIColor greenColor];
            break;
            
        default:
            break;
    }
    
    self.view.backgroundColor = color;
}

- (void)task01 {
    for (NSUInteger x = 0; YES; x++) {
        NSString *txt = [NSString stringWithFormat:@"%d", x];
        
        [_labelThread1 performSelectorOnMainThread:@selector(setText:)
                                        withObject:txt
                                     waitUntilDone:YES];
    }
}
@end
