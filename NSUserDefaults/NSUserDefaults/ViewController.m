//
//  ViewController.m
//  NSUserDefaults
//
//  Created by Formando FLAG on 25/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Instanciate NSUserDefaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    _firstNameTextField.text = [defaults objectForKey:@"FirstName"];
    _lastNameTextField.text = [defaults objectForKey:@"LastName"];
    _ageTextField.text = [defaults objectForKey:@"Age"];
    _imageView.image = [UIImage imageWithData:[defaults objectForKey:@"Photo" ]] ;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loadCamera:(UIBarButtonItem *)sender {
    //Configure picker
    UIImagePickerController *pickerVC = [[UIImagePickerController alloc]init];
    pickerVC.allowsEditing = YES;
    pickerVC.delegate = self;
    
    [self presentViewController:pickerVC animated:YES completion:^{}];
}

- (IBAction)save:(UIBarButtonItem *)sender {
    //Instanciate NSUserDefaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *photoData = UIImageJPEGRepresentation(_imageView.image, 1.0) ;

    //Save context info
    [defaults setObject:_firstNameTextField.text forKey:@"FirstName"];
    [defaults setObject:_lastNameTextField.text forKey:@"LastName"];
    [defaults setObject:_ageTextField.text forKey:@"Age"];
    [defaults setObject:photoData forKey:@"Photo"];

    //Force Writing
    [defaults synchronize];
}

#pragma Mark - UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return NO;
}

#pragma Mark - UIImagePickerControllerDelegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    _imageView.image = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    
}
@end
