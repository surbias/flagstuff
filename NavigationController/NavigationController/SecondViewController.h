//
//  SecondViewController.h
//  TabBar
//
//  Created by Formando FLAG on 23/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController
- (IBAction)showNextVC;
- (IBAction)goBack;

@end
