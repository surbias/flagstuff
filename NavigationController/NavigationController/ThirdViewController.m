//
//  ThirdViewController.m
//  TabBar
//
//  Created by Formando FLAG on 23/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "ThirdViewController.h"
@interface ThirdViewController ()

@end

@implementation ThirdViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil];
    
    if (self) {
        self.title = @"Third";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)goBackToRoot {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)goBackToRandom {
    NSArray *stack = self.navigationController.viewControllers;
    NSUInteger randomIndex = arc4random() % stack.count;
    UIViewController *randomVC = stack[randomIndex];
    
    [self.navigationController popToViewController:randomVC animated:YES];
}
@end
