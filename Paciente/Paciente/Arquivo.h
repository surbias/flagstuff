//
//  Arquivo.h
//  Paciente
//
//  Created by Formando FLAG on 04/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Paciente.h"
@interface Arquivo : Paciente

// Adiciona um paciente ao arquivo
-(void) adicionaPaciente:(Paciente *)umPaciente;

// Remove um paciente do arquivo
-(void) removePaciente:(Paciente *)umPaciente;

// Devolve uma array com o todos os pacientes em risco (abaixo
//de peso ou acima de peso)
-(NSArray *) pacientesEmRisco;

// Devolve um paciente com determinado número de Utente
-(Paciente *) pacienteComNumUtente:(NSString *)umNumUtente;

// Devolve todos os pacientes com um nome igual ao passado por
//argumento
-(NSArray *) pacientesChamados:(NSString *)umNome;

// Devolve a média do IMC de todos os pacientes no Arquivo
-(float) imcMedio;

// Devolve o número de pacientes guardados no Arquivo
-(NSUInteger) numPacientes;

@end
