//
//  Arquivo.m
//  Paciente
//
//  Created by Formando FLAG on 04/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "Arquivo.h"

@interface Arquivo()

@property(nonatomic, readwrite) NSMutableDictionary *pacientDictionary;

@end

@implementation Arquivo

- (instancetype)init{
    self = [super init];
    
    if (self) {
        _pacientDictionary = [[NSMutableDictionary alloc]init];
        
    }
    return self;
}

- (void)adicionaPaciente:(Paciente *)umPaciente{
    [_pacientDictionary setObject:umPaciente forKey:umPaciente.numUtente];
}

- (void)removePaciente:(Paciente *)umPaciente{
    [_pacientDictionary removeObjectForKey:umPaciente.numUtente];
}


- (NSArray *)pacientesEmRisco{
    NSMutableArray *tmp = [@[]mutableCopy];
    for (Paciente *paciente in [_pacientDictionary allValues]) {
        if ([paciente imc] > 25) {
            [tmp addObject:paciente];
        }
    }
    return [tmp copy];
}

- (Paciente *)pacienteComNumUtente:(NSString *)umNumUtente{
    return [_pacientDictionary objectForKey:umNumUtente];
}

- (NSArray *)pacientesChamados:(NSString *)umNome{
    NSMutableArray *tmp=[@[]mutableCopy];
    
    for (Paciente *paciente in [_pacientDictionary allValues]) {
        NSString *nomeCompleto = [NSString stringWithFormat:@"%@ %@",
                        paciente.primeiroNome, paciente.apelido];
        
        if ([nomeCompleto.uppercaseString containsString:umNome.uppercaseString]) {
            [tmp addObject:paciente];
        }
    }
    return [tmp copy];
}

- (float)imcMedio{
    float dictionaryCount = [_pacientDictionary count];
    float sumIMC = 0;
    for (Paciente *paciente in [_pacientDictionary allValues]) {
        sumIMC += [paciente imc];
    }
    
    return sumIMC / dictionaryCount;
}

- (NSUInteger)numPacientes{
    return [_pacientDictionary count];
}

@end
