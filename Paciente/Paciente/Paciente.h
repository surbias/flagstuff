//
//  Pacient.h
//  Paciente
//
//  Created by Formando FLAG on 04/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Paciente : NSObject

@property (nonatomic, readonly) NSString *primeiroNome;
@property (nonatomic, readonly) NSString *apelido;
@property (nonatomic, readonly) NSString *numUtente;
@property (nonatomic, readwrite) float altura;
@property (nonatomic, readwrite) float peso;

// Init
-(instancetype) initWithPrimeiroNome:(NSString *)pn
                             apelido:(NSString *)ap
                        andNumUtente:(NSString *)nu;
// Construtor
+(instancetype) pacienteWithPrimeiroNome:(NSString *)pn
                                 apelido:(NSString *)ap
                            andNumUtente:(NSString *)nu;

// Devolve o IMC do paciente
-(float)imc;

// Devolve uma descrição para o IMC do paciente
-(NSString *)imcDesc;

@end
