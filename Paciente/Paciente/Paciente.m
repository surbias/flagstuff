//
//  Pacient.m
//  Paciente
//
//  Created by Formando FLAG on 04/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "Paciente.h"

@implementation Paciente

- (instancetype)initWithPrimeiroNome:(NSString *)pn apelido:(NSString *)ap andNumUtente:(NSString *)nu{
    self = [super init];
    if (self) {
        _primeiroNome = pn;
        _apelido = ap;
        _numUtente = nu;
    }
    return self;
}

+ (instancetype)pacienteWithPrimeiroNome:(NSString *)pn apelido:(NSString *)ap andNumUtente:(NSString *)nu{
    return [[Paciente alloc]initWithPrimeiroNome:pn
                                         apelido:ap
                                    andNumUtente:nu];
}

- (float)imc{
    return _peso / (_altura * _altura);
}

- (NSString *)description{
    return [NSString stringWithFormat:@"Paciente: %@ \n Número de Utente: %@ \n IMC: %.2f",
            [NSString stringWithFormat:@"%@ %@", _primeiroNome, _apelido],
            _numUtente,
            [self imc]];
}

- (NSString *)imcDesc{
    
    
//    IMC	Classificação
//    < 16	Magreza grave
//    16 a < 17	Magreza moderada
//    17 a < 18,5	Magreza leve
//    18,5 a < 25	Saudável
//    25 a < 30	Sobrepeso
//    30 a < 35	Obesidade Grau I
//    35 a < 40	Obesidade Grau II (severa)
//    ≥ 40	Obesidade Grau III (mórbida)

    float imc = [self imc];
    if (imc < 18.5) {
        return @"Magreza moderada";
    } else if (imc > 18.5 && imc < 25){
        return @"Saudável";
    }else{
        return @"Resto";
    }
}

@end
