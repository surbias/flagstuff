//
//  main.m
//  Paciente
//
//  Created by Formando FLAG on 04/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Paciente.h"
#import "Arquivo.h"
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Arquivo *arquivo = [[Arquivo alloc] init];
        
        Paciente *paciente = [Paciente pacienteWithPrimeiroNome:@"Filipe"
                                                        apelido:@"Almeida"
                                                   andNumUtente:@"123456"];
        paciente.peso = 100;
        paciente.altura = 1.90;
        
        
        Paciente *paciente2 = [Paciente pacienteWithPrimeiroNome:@"El"
                                                        apelido:@"Surbias"
                                                   andNumUtente:@"32456"];
        paciente2.peso = 90;
        paciente2.altura = 1.85;
        
        [arquivo adicionaPaciente:paciente];
        [arquivo adicionaPaciente:paciente2];
        
        NSLog(@"%@", [arquivo pacienteComNumUtente:@"123456"]);
        
        NSLog(@"%.2f", [arquivo imcMedio]);
        
        NSLog(@"%@",[arquivo pacientesChamados:@"l Surbias"][0]);
        
        NSLog(@"%lu", [arquivo numPacientes]);
        
        NSLog(@"%@", paciente);
    }
    return 0;
}
