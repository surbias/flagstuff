//
//  MutablePerson.h
//  Person
//
//  Created by Formando FLAG on 02/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@interface MutablePerson : Person

@property(nonatomic, readwrite) NSString *firstName;
@property(nonatomic, readwrite) NSString *lastName;
@property(nonatomic, readwrite) NSNumber *age;

@end
