//
//  MutablePerson.m
//  Person
//
//  Created by Formando FLAG on 02/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "MutablePerson.h"

@implementation MutablePerson

//Searches the parent until it finds an implementation of the properties
@dynamic firstName, lastName, age;

@end
