//
//  Person.h
//  Person
//
//  Created by Formando FLAG on 02/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject <NSCopying, NSMutableCopying>

@property(nonatomic, readonly) NSString *firstName;
@property(nonatomic, readonly) NSString *lastName;
@property(nonatomic, readonly) NSNumber *age;


+(instancetype) personWithFirstName:(NSString *)aFirstName
                        andLastName:(NSString *)aLastName
                             andAge:(NSNumber *)anAge;

-(instancetype) initWithFirstName:(NSString *)aFirstName
                      andLastName:(NSString *)aLastName
                           andAge:(NSNumber *)anAge;

-(instancetype) initWithDictionary: (NSDictionary *)dict;


@end
