//
//  Person.m
//  Person
//
//  Created by Formando FLAG on 02/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "Person.h"
#import "MutablePerson.h"

@interface Person()

@property(nonatomic, readwrite) NSString *firstName;
@property(nonatomic, readwrite) NSString *lastName;
@property(nonatomic, readwrite) NSNumber *age;

@end


@implementation Person

- (instancetype)initWithFirstName:(NSString *)aFirstName andLastName:(NSString *)aLastName andAge:(NSNumber *)anAge{
    self = [super init];
    
    if (self) {
        _firstName = aFirstName;
        _lastName = aLastName;
        _age = anAge;
    }
    
    return self;
}

+ (instancetype)personWithFirstName:(NSString *)aFirstName andLastName:(NSString *)aLastName andAge:(NSNumber *)anAge{
    return [[Person alloc]initWithFirstName:aFirstName andLastName:aLastName andAge:anAge];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict{
    //alternate way
    self = [super init];
    
    if (self) {
        for (NSString *key in [dict allKeys]) {
            //key value coding
            [self setValue:dict[key] forKey:key];
        }
    }
    return self;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"\n Nome: %@ %@ \n Idade: %@", _firstName, _lastName, _age];
}

#pragma mark - NSCopying Methods
- (id)copyWithZone:(NSZone *)zone{
    return [[Person allocWithZone:zone]initWithFirstName:_firstName
                                             andLastName:_lastName
                                                  andAge:_age];
}

- (id)mutableCopyWithZone:(NSZone *)zone{
    return [[MutablePerson allocWithZone:zone]initWithFirstName:_firstName
                                             andLastName:_lastName
                                                  andAge:_age];
}

@end
