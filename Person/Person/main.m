//
//  main.m
//  Person
//
//  Created by Formando FLAG on 02/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        Person *person = [Person personWithFirstName:@"Filipe" andLastName:@"Almeida" andAge:@24];
        
        NSDictionary *dict = @{
                               @"firstName": @"Filipe",
                               @"lastName": @"Almeida",
                               @"age": @24
                             };
        
        Person *person2 = [[Person alloc]initWithDictionary:dict];
        
        NSLog(@"%@", person2);
        
        
        Person *clone = [person2 mutableCopy];
        NSLog(@"%@", clone);
    }
    return 0;
}
