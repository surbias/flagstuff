//
//  Item.h
//  Possessions
//
//  Created by Formando FLAG on 02/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Item <NSObject>

- (double)valueInDollars;

@optional
- (double)valueInEuros;

@end
