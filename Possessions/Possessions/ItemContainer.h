//
//  ItemContainer.h
//  Possessions
//
//  Created by Formando FLAG on 28/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"

@interface ItemContainer : NSObject

@property NSString *listName;

- (instancetype)initWithName:(NSString *) listName;
+ (instancetype)containerWithName:(NSString *) listName;

- (void)addItem:(id<Item>)item;
- (void)removeItem:(id<Item>)item;

@end
