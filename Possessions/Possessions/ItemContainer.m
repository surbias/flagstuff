//
//  ItemContainer.m
//  Possessions
//
//  Created by Formando FLAG on 28/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "ItemContainer.h"

@interface ItemContainer()

@property NSMutableArray *items;

- (double) totalPrice;
@end

@implementation ItemContainer

- (instancetype)initWithName:(NSString *) listName{
    self = [super init];
    
    if (self) {
        _listName = listName;
        _items = [@[] mutableCopy];
    }
    return self;
}

+ (instancetype)containerWithName:(NSString *) listName{
    return [[ItemContainer alloc]initWithName:listName];
}


- (void)addItem:(id<Item>)item{
    [_items addObject:item];
}
- (void)removeItem:(id<Item>)item{
    [_items removeObject:item];
}

- (double) totalPrice{
    double total = 0;
    for (id<Item> item in _items) {
        total += item.valueInDollars;
    }
    return total;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"Name: %@ \n Number of Items: %lu \n Total Value: $%.2f",
            _listName, [_items count], [self totalPrice]];
}

@end
