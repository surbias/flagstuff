//
//  RandomItem.h
//  Possessions
//
//  Created by Formando FLAG on 28/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"

@interface RandomItem : NSObject <Item>

@property NSString *itemName;
@property NSString *serialNumber;
@property double valueInDollars;
@property (readonly) NSDate *dateCreated;

- (instancetype)init;

//Convenience Constructor
+ (instancetype)randomItem;


@end
