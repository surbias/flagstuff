//
//  RandomItem.m
//  Possessions
//
//  Created by Formando FLAG on 28/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//
#import "RandomItem.h"

@implementation RandomItem

+ (instancetype)randomItem{
    return [[RandomItem alloc]init];
}

- (instancetype)init{
    self = [super init];
    
    if (self) {
        //Initialize ivars
        _dateCreated = [NSDate date];
        _valueInDollars = (arc4random() % 100) + 1;
        _serialNumber = [NSString stringWithFormat:@"%x", arc4random()];
        
        
        NSArray *adjectives = @[@"Rusty", @"Fluffy", @"Shiny"];
        NSArray *nouns = @[@"Mac", @"Spork", @"Bear"];
        
        NSUInteger randomIndexAdjectives = arc4random() % 100 % [adjectives count];
        NSUInteger randomIndexNouns = arc4random() % 100 % [nouns count];
        
        _itemName = [NSString stringWithFormat:@"%@ %@",
                     adjectives[randomIndexAdjectives],
                     nouns[randomIndexNouns]];
    }
    return self;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"%@ [%@] - $%.2f",
            _itemName, _serialNumber, _valueInDollars];
}

#pragma mark - Item methods
- (double)valueInEuros{
    return _valueInDollars * 1.2;
}

@end
