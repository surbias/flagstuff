//
//  main.m
//  Possessions
//
//  Created by Formando FLAG on 28/05/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RandomItem.h"
#import "ItemContainer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        //NSLog(@"%@", [RandomItem randomItem]);
        RandomItem * item01 = [RandomItem randomItem];
        RandomItem * item02 = [RandomItem randomItem];
        
        ItemContainer *container = [ItemContainer containerWithName:@"My Container"];
        [container addItem:item01];
        [container addItem:item02];
        
        NSLog(@"%@", container);
        
        NSLog(@"%f", [item01 valueInDollars]);
        NSLog(@"%f", [item01 valueInEuros]);
    }
    return 0;
}
