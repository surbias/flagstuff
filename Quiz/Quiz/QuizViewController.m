//
//  QuizViewController.m
//  Quiz
//
//  Created by Formando FLAG on 11/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "QuizViewController.h"

@interface QuizViewController ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
- (IBAction)nextQuestion;
- (IBAction)showAnswer;

@property(strong, nonatomic) NSArray *questions;
@property(strong, nonatomic) NSArray *answers;

@property int current_counter;

@end

@implementation QuizViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _questions = @[
                   @"Qual é o teu nome?",
                   @"Qual a tua nacionalidade?",
                   @"Qual a tua linguagem de programação favorita?"
                   ];
    
    _answers = @[
                 @"Napoleão",
                 @"Portuguesa",
                 @"Python"
                 ];
    
    
    _current_counter = 0;
    _questionLabel.text = _questions[_current_counter];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 
- (IBAction)nextQuestion {
    
    _current_counter = (_current_counter + 1) % [_questions count];
    _answerLabel.text = @"...";
    _questionLabel.text = _questions[_current_counter];
    
    
}

- (IBAction)showAnswer {
    _answerLabel.text = _answers[_current_counter];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
