//
//  main.m
//  Reflection
//
//  Created by Formando FLAG on 09/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Camelleon.h"
@class Reptile;
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        
        Camelleon *camelleon = [[Camelleon alloc]init];
        
        //check if an instance belongs to a certain class or any of its parents
        if ([camelleon isKindOfClass:[Reptile class]]) {
            NSLog(@"is a reptile");
        }
        
        //check if an belongs to a class that implements a certain protocol
        NSString *str = @"ABC";
        if ([str conformsToProtocol:@protocol(NSCopying)]) {
            NSLog(@"str implements NSCopying Protocol");
        }
        
        //check if an instance implements a method
        if ([str respondsToSelector:@selector(isEqualToString:)]) {
            NSLog(@"str has method isEqualToString");
        }
        
        //Reflection
        SEL method = @selector(lowercaseString);
        if ([str respondsToSelector:method]) {
            str = [str performSelector:method];
            NSLog(@"%@", str);
        }
        
        NSLog(@"%@", [camelleon className]);
        
    }
    return 0;
}
