//
//  RestAUX.h
//  RestWebServices
//
//  Created by Formando FLAG on 07/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RestAUXInvoker <NSObject>

-(void) receivedFahrenheit:(double)fahrenheitValue;

@end

@interface RestAUX : NSObject<RestAUXInvoker>

//Return web service's url
+(NSURL *) serviceURL;

//Calls web service and returns the answer
+(void) celsiusToFahrenheit:(double) value andCallBackTo:(id<RestAUXInvoker>)invoker;



@end
