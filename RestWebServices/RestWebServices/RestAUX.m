//
//  RestAUX.m
//  RestWebServices
//
//  Created by Formando FLAG on 07/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "RestAUX.h"
#import "XMLReader.h"

@implementation RestAUX

//Return web service's url
+(NSURL *) serviceURL{
    
    return [NSURL URLWithString:@"http://www.w3schools.com/webservices/tempconvert.asmx/CelsiusToFahrenheit"];
}

//Calls web service and returns the answer
+(void) celsiusToFahrenheit:(double) value andCallBackTo:(id<RestAUXInvoker>)invoker{
    
    static NSOperationQueue *queue = nil;
    //Keeps the queue in memory
    if (queue == nil) {
        queue = [[NSOperationQueue alloc]init];
    }
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[RestAUX serviceURL]];
    
    //HTTP Body
    NSString *bodyString = [NSString stringWithFormat:@"Celsius=%.2f", value];
    urlRequest.HTTPBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    //HTTP Header
    NSString *bodyLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[urlRequest.HTTPBody length]];
    
    //Set Content Length
    [urlRequest setValue:bodyLengthString forHTTPHeaderField:@"Content-Length"];
    
    //Set content type
    [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //Set HTTP Method
    [urlRequest setHTTPMethod:@"POST"];
    
    
    void(^HTTPRequestOperation)() = ^() {
        //Executes the request
        NSHTTPURLResponse *response;
        NSError *error;
        
        NSData *xmlData = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
        
        if (response.statusCode != 200 || error != nil) {
            NSLog(@"CelsiusToFahrenheit HTTP: %@", response);
            NSLog(@"CelsiusToFahrenheit Error: %@", [error localizedDescription]);
            
            return;
        }
        
        NSDictionary *data =[XMLReader dictionaryForXMLData:xmlData error:&error];
        
        NSLog(@"%@", [[NSString alloc] initWithData:xmlData encoding:NSUTF8StringEncoding]);
        
        //Summons the callback on the main thread
        if([invoker respondsToSelector:@selector(receivedFahrenheit:)]){
            //return the converted value
            dispatch_async(dispatch_get_main_queue(), ^{
                [invoker receivedFahrenheit:[data[@"string"][@"text"] doubleValue]];
            });
        }
    };
    
    //create operation
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:HTTPRequestOperation];
    
    //add operation to the queue
    [queue addOperation:operation];
    
    
}


@end
