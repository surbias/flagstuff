   //
//  ViewController.m
//  RestWebServices
//
//  Created by Formando FLAG on 07/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "ViewController.h"
#import "RestAUX.h"

@interface ViewController ()<RestAUXInvoker>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)go {
    [RestAUX celsiusToFahrenheit:[_celsiusTextField.text doubleValue] andCallBackTo:self];
}

#pragma mark - RestAUXInvoker Methods
- (void)receivedFahrenheit:(double)fahrenheitValue {
    
    _fahrenheitTextField.text = [NSString stringWithFormat:@"%.2f", fahrenheitValue];
}
@end
