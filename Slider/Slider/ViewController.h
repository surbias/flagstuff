//
//  ViewController.h
//  Slider
//
//  Created by Formando FLAG on 11/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *squareView;

- (IBAction)changeColor:(UISlider *)sender;

@end

