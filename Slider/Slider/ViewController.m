//
//  ViewController.m
//  Slider
//
//  Created by Formando FLAG on 11/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _squareView.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)changeColor:(UISlider *)sender {
    CGFloat red, blue, green, alpha;
    [_squareView.backgroundColor getRed:&red green:&green blue:&blue alpha:&alpha];
    switch (sender.tag) {
        case 0:
            red = sender.value;
            break;
        case 1:
            green = sender.value;
            break;
        case 2:
            blue = sender.value;
            break;
            
        default:
            break;
    }
    _squareView.backgroundColor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}
@end
