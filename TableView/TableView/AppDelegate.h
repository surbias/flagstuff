//
//  AppDelegate.h
//  TableView
//
//  Created by Formando FLAG on 30/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

