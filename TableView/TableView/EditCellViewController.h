//
//  EditCellViewController.h
//  TableView
//
//  Created by Formando FLAG on 30/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"

@interface EditCellViewController : UIViewController



@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *valueTextField;
@property(strong, nonatomic) Item *selectedItem;

- (IBAction)save:(UIBarButtonItem *)sender;
@end
