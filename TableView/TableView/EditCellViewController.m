//
//  EditCellViewController.m
//  TableView
//
//  Created by Formando FLAG on 30/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "EditCellViewController.h"

@interface EditCellViewController ()

@end

@implementation EditCellViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_selectedItem){
        self.navigationItem.title = _selectedItem.itemName;
        _nameTextField.text = _selectedItem.itemName;
        _valueTextField.text = [NSString stringWithFormat:@"%d", _selectedItem.valueInDollars];
    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)save:(UIBarButtonItem *)sender {
    _selectedItem.itemName = _nameTextField.text;
    _selectedItem.valueInDollars = [_valueTextField.text intValue];
    
}
@end
