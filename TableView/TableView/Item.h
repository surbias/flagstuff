//
//  Item.h
//  Possessions
//
//  Created by João D. Moreira on 5/18/13.
//  Copyright (c) 2013 Goldnpot. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject

@property (readwrite) NSString *itemName;
@property (readwrite) NSString *serialNumber;
@property (readwrite) int valueInDollars;
@property (readonly)  NSDate *dateCreated;

+ (id)randomItem; // construtores de conveniencia

- (id)initWithItemName:(NSString *)name // init
        valueInDollars:(int)value
          serialNumber:(NSString *)sNumber;

@end
