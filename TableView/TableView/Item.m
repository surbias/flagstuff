//
//  Item.m
//  Possessions
//
//  Created by João D. Moreira on 5/18/13.
//  Copyright (c) 2013 Goldnpot. All rights reserved.
//

#import "Item.h"

@implementation Item

+ (id)randomItem {
    
    // Cria um array com adjectivos
    NSArray *randomAdjectiveList = [NSArray arrayWithObjects:@"Fluffy", @"Rusty", @"Shiny", nil];
    
    // Cria um array com os nomes
    NSArray *randomNounList = [NSArray arrayWithObjects:@"Bear", @"Spork", @"Mac", nil];

    // Gera dois indices de forma aleatoria
    NSInteger adjectiveIndex = arc4random() % [randomAdjectiveList count];
    NSInteger nounIndex = arc4random() % [randomNounList count];
    
    // Combina um dos adjectivos com um dos nomes
    NSString *randomName = [NSString stringWithFormat:@"%@ %@",
                            [randomAdjectiveList objectAtIndex:adjectiveIndex],[randomNounList objectAtIndex:nounIndex]];
    
    // Gera o valor de forma aleatoria entre $0 e $100
    int randomValue = arc4random() % 100;
    
    
    // Gera o numero aleatorio
    NSString *randomSerialNumber = [NSString stringWithFormat:@"%c%c%c%c%c",
                                    '0' + arc4random() % 10,
                                    'A' + arc4random() % 26,
                                    '0' + arc4random() % 10,
                                    'A' + arc4random() % 26,
                                    '0' + arc4random() % 10];
    
    
    return [[Item alloc] initWithItemName:randomName valueInDollars:randomValue serialNumber:randomSerialNumber];
}

- (id)initWithItemName:(NSString *)name valueInDollars:(int)value serialNumber:(NSString *)sNumber {
    
    self = [super init];
    
    if(self != nil) {
        _itemName = name;
        _valueInDollars = value;
        _serialNumber = sNumber;
        _dateCreated = [NSDate date];
    }
    
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@, %@ - $%d", self.itemName, self.serialNumber, self.valueInDollars];
}

@end
