//
//  ItemsList.h
//  Possessions
//
//  Created by João D. Moreira on 5/18/13.
//  Copyright (c) 2013 Goldnpot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"

@interface ItemContainer : NSObject

@property NSString *listName;
@property NSMutableArray *itemsList;

+(id) containerWithName:(NSString *) listName; // construtor de conveniencia
-(id) initWithName:(NSString *) listName; // init

// Metodos que adicionam/removem items à lista
-(void) addItem:(Item *)item;
-(void) removeItem:(Item *)item;

// Metodo que devolve o preco total dos items na lista
-(int) totalPrice;

// Metodo que devolve o numero de Possessions no container
-(int) numberOfPossessions;

@end
