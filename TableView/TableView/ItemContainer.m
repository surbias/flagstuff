//
//  ItemsList.m
//  Possessions
//
//  Created by João D. Moreira on 5/18/13.
//  Copyright (c) 2013 Goldnpot. All rights reserved.
//

#import "ItemContainer.h"

@implementation ItemContainer

- (id)initWithName:(NSString *)listName {
    self = [super init];
    
    if(self) {
        _listName = listName;
        _itemsList = [NSMutableArray array];
    }
    
    return self;
}

+(id)containerWithName:(NSString *) listName {
    return [[ItemContainer alloc] initWithName:listName];
}

- (void)addItem:(Item *)item {
    [_itemsList addObject:item];
}

- (void)removeItem:(Item *)item {
    [_itemsList removeObject:item];
}

- (int)totalPrice {
    
    int acumulador = 0;
    for (Item *xpto in _itemsList) {
        acumulador += xpto.valueInDollars;
    }
    
    return acumulador;
}

- (int) numberOfPossessions {
    return [self.itemsList count];
}

@end
