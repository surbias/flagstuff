//
//  TableViewController.h
//  TableView
//
//  Created by Formando FLAG on 30/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController

- (IBAction)addNewItem:(id)sender;
- (IBAction)toogleEdit:(id)sender;

@end
