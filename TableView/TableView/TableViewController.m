//
//  TableViewController.m
//  TableView
//
//  Created by Formando FLAG on 30/06/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "TableViewController.h"
#import "EditCellViewController.h"

#import "ItemContainer.h"
#import "Item.h"


@interface TableViewController ()

@property(strong, nonatomic) ItemContainer *my;
@property(strong, nonatomic) ItemContainer *our;
@property(strong, nonatomic) ItemContainer *your;

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _my = [[ItemContainer alloc]initWithName:@"My Possessions"];
    _our = [[ItemContainer alloc]initWithName:@"Our Possessions"];
    _your = [[ItemContainer alloc]initWithName:@"Your Possessions"];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//NSUInteger totalCells = [_my numberOfPossessions] + [_your numberOfPossessions] + [_our numberOfPossessions];
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[self containerForSection:section] numberOfPossessions];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    ItemContainer *thisContainer = [self containerForSection:section];
    return [NSString stringWithFormat:@"%@ - $%d.00", thisContainer.listName, [thisContainer totalPrice]];
    //[self containerForSection:section].listName;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    return @"---- ---- ---- ---- ---- ---- ---- ----";
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    
    
    Item *thisItem = [self itemForSection:section andRow:row];
    
    cell.textLabel.text = thisItem.itemName;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"$%d.00", thisItem.valueInDollars];
    
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source

        ItemContainer *thisContainer = [self containerForSection:indexPath.section];
        Item *thisItem = [self itemForSection:indexPath.section andRow:indexPath.row];
        [thisContainer removeItem:thisItem];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    [self.tableView reloadData];
}



// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {

    //Delete From Origin
    ItemContainer *thisContainer = [self containerForSection:fromIndexPath.section];
    Item *thisItem = [self itemForSection:fromIndexPath.section andRow:fromIndexPath.row];
    
    [thisContainer removeItem:thisItem];
    
    ItemContainer *destContainer = [self containerForSection:toIndexPath.section];
    
    [destContainer.itemsList insertObject:thisItem atIndex:toIndexPath.row];
    
    [self.tableView reloadData];
    
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"goToDetail"]) {
        EditCellViewController *detail = segue.destinationViewController;
        NSUInteger row = self.tableView.indexPathForSelectedRow.row;
        NSUInteger section = self.tableView.indexPathForSelectedRow.section;
        detail.selectedItem = [self itemForSection:section andRow:row];

    }
    
    
}


- (IBAction)addNewItem:(id)sender {
    [_my addItem:[Item randomItem]];
    [self.tableView reloadData];
}

- (IBAction)toogleEdit:(id)sender {
    self.tableView.editing = !self.tableView.editing;
}

#pragma mark - Helper Methods
- (ItemContainer *) containerForSection:(NSUInteger)section{
    ItemContainer *thisContainer;
    
    switch (section) {
        case 0:
            thisContainer = _my;
            break;
        case 1:
            thisContainer = _your;
            break;
        case 2:
            thisContainer = _our;
            break;
    }

    return thisContainer;
}

- (Item *) itemForSection:(NSUInteger)section andRow:(NSUInteger)row{
    return [self containerForSection:section].itemsList[row];
}

@end
